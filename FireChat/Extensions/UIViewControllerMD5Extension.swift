//
//  UIViewControllerMD5Extension.swift
//  FireChat
//
//  Created by Cohen, Dor on 05/10/2020.
//

import UIKit
import CryptoKit

extension UIViewController {
    func MD5(string: String) -> String {
        let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())

        return digest.map {
            String(format: "%02hhx", $0)
        }.joined()
    }
}
