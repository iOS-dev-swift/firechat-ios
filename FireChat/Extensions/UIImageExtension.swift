//
//  UIImageExtension.swift
//  FireChat
//
//  Created by Cohen, Dor on 04/10/2020.
//

import UIKit

extension UIImage {
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func getScaledDimensions(toWidth: CGFloat){
        let width = self.size.width
        let height = self.size.height
        let viewWidth = toWidth * 0.8
        let ratio = viewWidth/width
        let scaledHeight = height * ratio
        print(width,height,viewWidth,scaledHeight)
    }
    
}
