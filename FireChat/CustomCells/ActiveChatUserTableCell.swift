//
//  ActiveChatUserTableCell.swift
//  FireChat
//
//  Created by Cohen, Dor on 05/10/2020.
//

import UIKit

class ActiveChatUserTableCell : UITableViewCell{
    /*
    let usernameLabel = UILabel()
    let messageLabel  = UILabel()
    let profilePicture = UIImageView()
     */
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var unreadMessages: UILabel!
    /*
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        usernameLabel.numberOfLines = 1
        usernameLabel.lineBreakMode = .byTruncatingTail
        usernameLabel.textAlignment = .left
        usernameLabel.sizeToFit()
        
        messageLabel.numberOfLines = 1
        messageLabel.lineBreakMode = .byTruncatingTail
        messageLabel.textAlignment = .left
        messageLabel.sizeToFit()
        
        profilePicture.contentMode = .scaleAspectFit
        
        contentView.addSubview(usernameLabel)
        contentView.addSubview(messageLabel)
        contentView.addSubview(profilePicture)
        
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        profilePicture.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            profilePicture.widthAnchor.constraint(equalToConstant: 250),
            profilePicture.heightAnchor.constraint(equalTo: profilePicture.widthAnchor, multiplier: 1),
            profilePicture.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            profilePicture.trailingAnchor.constraint(equalTo: usernameLabel.leadingAnchor,constant: 50),
            profilePicture.trailingAnchor.constraint(equalTo: messageLabel.leadingAnchor,constant: 50),

            usernameLabel.leadingAnchor.constraint(equalTo: profilePicture.trailingAnchor, constant: 50),
            usernameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            usernameLabel.bottomAnchor.constraint(equalTo: messageLabel.topAnchor),

            messageLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor),
            messageLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            messageLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            messageLabel.leadingAnchor.constraint(equalTo: profilePicture.trailingAnchor, constant: 50)
        ])
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
     */
}
