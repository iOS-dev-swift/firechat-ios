//
//  RightMessagePhotoTableCell.swift
//  FireChat
//
//  Created by Cohen, Dor on 07/10/2020.
//

import UIKit
import PaddingLabel

class RightMessagePhotoTableCell : UITableViewCell{
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var message: PaddingLabel!
}
