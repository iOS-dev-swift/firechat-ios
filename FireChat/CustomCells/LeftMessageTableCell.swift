//
//  LeftMessageTableCell.swift
//  FireChat
//
//  Created by Cohen, Dor on 04/10/2020.
//

import UIKit
import PaddingLabel

class LeftMessageTableCell : UITableViewCell{
    @IBOutlet weak var message: PaddingLabel!
    /*
    let label = PaddingLabel(frame: CGRect(x: 0, y: 0, width: 500, height: 100))
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0))
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.layer.borderWidth = 1
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        label.numberOfLines = 0
        label.lineBreakMode = .byCharWrapping
        label.topInset = 15
        label.bottomInset = 15
        label.leftInset = 15
        label.rightInset = 15
        label.textAlignment = .left
        label.layer.backgroundColor = #colorLiteral(red: 0.7019608021, green: 0.8431372643, blue: 1, alpha: 1)
        label.sizeToFit()
        
        contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.widthAnchor.constraint(lessThanOrEqualTo: contentView.widthAnchor, multiplier: 0.8),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    */
}
