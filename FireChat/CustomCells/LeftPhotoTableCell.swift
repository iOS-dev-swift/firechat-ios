//
//  LeftPhotoTableCell.swift
//  FireChat
//
//  Created by Cohen, Dor on 04/10/2020.
//

import UIKit

class LeftPhotoTableCell : UITableViewCell{
    

    @IBOutlet weak var imageview: UIImageView!
    /*
    let imageview = UIImageView()
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0))
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.layer.cornerRadius = 10
        imageview.clipsToBounds = true
        imageview.layer.borderWidth = 0
        imageview.layer.masksToBounds = true
        
        contentView.addSubview(imageview)
        
        NSLayoutConstraint.activate([
             imageview.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
             imageview.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
             imageview.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    */
}
