//
//  ChatsTableViewController.swift
//  FireChat
//
//  Created by Cohen, Dor on 26/09/2020.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import CoreData

class ChatsTableViewController: UITableViewController {

    @IBOutlet weak var logoutButton: UIBarButtonItem!
    @IBAction func logoutButtonTapped(_ sender: Any) {
        try? Auth.auth().signOut()
        dismiss(animated: true, completion: nil)
    }
    
    var context : NSManagedObjectContext?
    var chats : [chat] = []
    var dbHandler : DatabaseHandle?
    let databaseRef = Database.database().reference()

    
    override func viewWillAppear(_ animated: Bool) {
        getChats()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.register(ActiveChatUserTableCell.self, forCellReuseIdentifier: "ActiveChatUserTableCell")
        tableView.rowHeight          = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        //MARK: - Create CoreData context
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        self.context = appDelegate.persistentContainer.viewContext
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let dbHandler = dbHandler{
            databaseRef.removeObserver(withHandle: dbHandler)
        }
    }
    
    @objc func getChats(){
        chats = []
        // MARK: - Get chats from CoreData Internal storage
        let request = StoredChats.fetchRequest() as NSFetchRequest<StoredChats>
        //let sort = NSSortDescriptor(key: "messages.date", ascending: true)
        //request.sortDescriptors = [sort]
        
        if let context = context{
            if let storedChats = try? context.fetch(request) as [StoredChats]{
                for storedChat in storedChats{
                    let setMessages = storedChat.messages as? Set<StoredMessages> ?? []
                    let sortedMessages = setMessages.sorted { $0.date! < $1.date! }
                    chats.append(chat(messages: sortedMessages, chat: storedChat))
                }
            }
        }
        
        self.reloadChats(tableView)
    }
    
    @objc func reloadChats(_ view : UITableView){
        view.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ActiveChatUserCell = tableView.dequeueReusableCell(withIdentifier: "activechatcell", for: indexPath) as! ActiveChatUserTableCell
        
        let chat = chats[indexPath.row]
        
        ActiveChatUserCell.unreadMessages.isHidden = true
        ActiveChatUserCell.usernameLabel.text = chat.chat.name
        
        if let lastMessage = chat.messages.last{
            // MARK: - Get messages from FireBase Database
            let dbRef = databaseRef.child("messages").child(Auth.auth().currentUser!.uid).child(chat.chat.chatId!)
            dbHandler = dbRef.observe(.value) { snapshot in
                var unreadMessages : [String] = []
                for child in snapshot.children{
                    let snap = child as! DataSnapshot
                    if let record = snap.value as? [String:Any]{
                        let message = record["message"] as? String ?? ""
                        if message.isEmpty{
                            ActiveChatUserCell.messageLabel.text = lastMessage.body
                        }else{
                            unreadMessages.append(message)
                            ActiveChatUserCell.unreadMessages.text! = String(unreadMessages.count)
                            ActiveChatUserCell.unreadMessages.isHidden = false
                            ActiveChatUserCell.messageLabel.text = message
                        }
                    }
                }
            }
        }
        return ActiveChatUserCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToViewChat", sender: chats[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let chatVC = segue.destination as? ChatViewController{
            if let chat = sender as? chat{
                chatVC.chat = chat
            }
        }
    }

}
