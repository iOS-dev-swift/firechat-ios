//
//  RegisterViewController.swift
//  FireChat
//
//  Created by Cohen, Dor on 26/09/2020.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var usernameTextbox: UITextField!
    @IBOutlet weak var passwordTextbox: UITextField!
    @IBOutlet weak var retypePasswordTextbox: UITextField!
    @IBOutlet weak var emailTextbox: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func RegisterTapped(_ sender: Any) {
        
        guard passwordTextbox.text! == retypePasswordTextbox.text! else{
            let alert = UIAlertController(title: "Error", message: "Passwords do not match.", preferredStyle: .alert)
                 let ok = UIAlertAction(title: "OK", style: .default, handler: { action in })
                 alert.addAction(ok)
                 DispatchQueue.main.async(execute: { self.present(alert, animated: true) })
            return
        }
        
        guard usernameTextbox.hasText else{
            let alert = UIAlertController(title: "Error", message: "Display Name must be provided", preferredStyle: .alert)
                 let ok = UIAlertAction(title: "OK", style: .default, handler: { action in })
                 alert.addAction(ok)
                 DispatchQueue.main.async(execute: { self.present(alert, animated: true) })
            return
        }
        
        Auth.auth().createUser(withEmail: emailTextbox.text!, password: passwordTextbox.text!, completion: { (authDataResult, error) in
            if let error = error{
                let error = error as NSError
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                     let ok = UIAlertAction(title: "OK", style: .default, handler: { action in })
                     alert.addAction(ok)
                     DispatchQueue.main.async(execute: { self.present(alert, animated: true) })
            }else{
                let user = authDataResult!.user
                let changeRequest = user.createProfileChangeRequest()
                changeRequest.displayName = self.usernameTextbox.text
                changeRequest.commitChanges(completion: { (status) in
                    let ref = Database.database().reference()
                    ref.child("users").child(user.uid).setValue(
                        ["email": self.emailTextbox.text!, "displayName" : self.usernameTextbox.text!]
                    )
                })
                user.sendEmailVerification(completion: nil)
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }

}
