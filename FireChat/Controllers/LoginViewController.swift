//
//  LoginViewController.swift
//  FireChat
//
//  Created by Cohen, Dor on 26/09/2020.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextbox: UITextField!
    @IBOutlet weak var passwordTextbox: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func loginTapped(_ sender: Any) {
        Auth.auth().signIn(withEmail: usernameTextbox.text!, password: passwordTextbox.text!) { (authDataResult, error) in
            if let error = error{
                let error = error as NSError
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                     let ok = UIAlertAction(title: "OK", style: .default, handler: { action in })
                     alert.addAction(ok)
                     DispatchQueue.main.async(execute: { self.present(alert, animated: true) })
            }else{
                self.performSegue(withIdentifier: "goToChats", sender: nil)
            }
        }
    }
    
    
    @IBAction func registerTapped(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }

}
