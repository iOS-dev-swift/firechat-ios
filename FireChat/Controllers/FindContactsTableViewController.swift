//
//  FindContactsTableViewController.swift
//  FireChat
//
//  Created by Cohen, Dor on 30/09/2020.
//

import UIKit
import CoreData
import FirebaseDatabase
import FirebaseAuth

class FindContactsTableViewController: UITableViewController {
    
    
    var users : [StoredUsers] = []
    var filteredUsers : [StoredUsers] = []
    let searchController = UISearchController(searchResultsController: nil)
    var context : NSManagedObjectContext!
    
    //MARK: - SearchBar check if search is empty
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    //MARK: - SearchBar filter content
    func filterContentForSearchText(_ searchText: String,
                                    category: StoredUsers? = nil) {
        filteredUsers = users.filter { (user: StoredUsers) -> Bool in
            return user.displayName!.lowercased().contains(searchText.lowercased())
        }
      
        tableView.reloadData()
    }
    
    //MARK: - SearchBar check if filter applies
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }

    
    // MARK: - Get a list of users from FireBase Server
    @objc func getListOfUsers(){
        let ref = Database.database().reference()
        let userID = Auth.auth().currentUser!.uid
        users = []
        
        ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for child in snapshots {
                    if let userInfo = child.value as? NSDictionary{
                        if child.key != userID{
                            let storedUser = StoredUsers(entity: StoredUsers.entity(), insertInto: nil)
                            storedUser.displayName = userInfo["displayName"] as? String
                            storedUser.email = userInfo["email"] as? String
                            storedUser.uid = child.key
                            self.users.append(storedUser)
                        }
                    }
                }
            }
            
            self.tableView.reloadData()
        })
    }
    
     override func viewWillAppear(_ animated: Bool) {
        getListOfUsers()
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //MARK: - Create CoreData context
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        context = appDelegate.persistentContainer.viewContext
        
        //MARK: - SearchBar delegtions
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Users"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredUsers.count
        }
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        if isFiltering{
            cell.textLabel!.text = "\(filteredUsers[indexPath.row].displayName!) - \(filteredUsers[indexPath.row].email!)"
        }else{
            cell.textLabel!.text = "\(users[indexPath.row].displayName!) - \(users[indexPath.row].email!)"
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToViewChat", sender: users[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let chatVC = segue.destination as? ChatViewController{
            if let user = sender as? StoredUsers{
                //MARK: - Check if a user exist in CoreData, if not - create on
                let userRequest = StoredUsers.fetchRequest() as NSFetchRequest<StoredUsers>
                let userPredicate = NSPredicate(format: "uid == %@", user.uid!)
                userRequest.predicate = userPredicate
                if let storedUsers = try? context.fetch(userRequest) as [StoredUsers]{
                    if storedUsers.isEmpty{
                        // Create the stored chat
                        let storedChat = StoredChats(entity : StoredChats.entity(),insertInto: context)
                        storedChat.chatId = user.uid
                        storedChat.name = user.displayName
                        storedChat.creationDate = Date()
                        // Create the stored user
                        let storedUser = StoredUsers(entity : StoredUsers.entity(),insertInto: context)
                        storedUser.displayName = user.displayName
                        storedUser.email = user.email
                        storedUser.uid = user.uid
                        storedChat.creationDate = Date()
                        storedUser.addToChat(storedChat)
                        try! context.save()
                        
                        chatVC.chat = chat(messages: [], chat: storedChat)
                    }else{
                        if let userChat = storedUsers.first?.chat{
                            let storedChat = userChat.allObjects as! [StoredChats]
                            if let storedChat = storedChat.first{
                                let setMessages = storedChat.messages as? Set<StoredMessages> ?? []
                                let sortedMessages = setMessages.sorted { $0.date! < $1.date! }
                                chatVC.chat = chat(messages: sortedMessages, chat: storedChat)
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK: - SearchBar delegte
extension FindContactsTableViewController: UISearchResultsUpdating {
    
    //MARK: - SearchBar update the search result functionality
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
