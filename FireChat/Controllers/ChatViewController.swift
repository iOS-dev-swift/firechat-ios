//
//  ChatViewController.swift
//  FireChat
//
//  Created by Cohen, Dor on 28/09/2020.
//

import UIKit
import CoreData
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class ChatViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate {
    
    var imagePicker = UIImagePickerController()
    let dateFormatter = DateFormatter()
    
    var chat : chat!
    var storedMessages : [StoredMessages] = []{
        didSet {
                DispatchQueue.main.async {
                    self.reloadMessages()
                }
            }
    }
    var storedFriend : StoredUsers!
    var storedUser : StoredUsers!
    
    var databaseRef: DatabaseReference?
    var storageRef: StorageReference?
    var context : NSManagedObjectContext?
    var dbHandler : DatabaseHandle?
    
    // MARK: - Create the message box textField image elements
    let messageImageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 50, height: 50))
    let messageDeleteImageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    let messageDeleteImage = UIImage(systemName: "delete.right.fill")
    let messageLeftView = UIView.init(frame: CGRect(x: 10, y: 0, width: 70, height: 70))
    var messageHeightConstraint : NSLayoutConstraint = NSLayoutConstraint()
    

    @IBOutlet weak var chatsTableView: UITableView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var imagesButton: UIBarButtonItem!
    @IBOutlet weak var userDisplayNameLabel: UILabel!
    
    // MARK: - Photo album tapped functionality -> Open the photo album
    @IBAction func imagesTapped(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Camera tapped functionality -> Open the camera
    @IBAction func cameraTapped(_ sender: Any) {
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Get called when a user picks an image on the camera or photo album
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[.originalImage] as? UIImage{
            
            // MARK: - Attach the selected picture to the UIImage object and activate the height constraint
            messageImageView.image = selectedImage
            messageHeightConstraint.isActive = true
            messageTextField.leftViewMode = UITextField.ViewMode.always
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Invoke when deleteImageButton is tapped, disable height constraint and leftView
    @objc private func deleteImageAction(){
        messageTextField.leftViewMode = UITextField.ViewMode.never
        messageHeightConstraint.isActive = false
        messageImageView.image = nil
    }
    
    // MARK: - Submit tapped functionality -> Submit the message and/or data
    @IBAction func submitTapped(_ sender: Any) {
        submitMessage()
    }
    
    // MARK: - Store data into FireBase server
    func saveToServer(message: String = String(), data: Data = Data(), localIndex: Int){
        guard let storageRef = storageRef else { return }
        guard let databaseRef = databaseRef else { return }
        let hashId = MD5(string: String(dateFormatter.string(from: Date())))
        let dataRef = storageRef.child("images").child(storedFriend.uid!).child(storedUser.uid!).child(hashId)
        let dbRef = databaseRef.child("messages").child(storedFriend.uid!).child(storedUser.uid!).child(dateFormatter.string(from: Date()))
        
        // MARK: - Store data into FireBase storage
        if !data.isEmpty{
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            dataRef.putData(data, metadata: metadata) { (metadata, error) in
                dataRef.downloadURL { (url, error) in
                    if let url = url{
                        // Store the URL reference (url.description), hashId and message to FireBase database
                        if !message.isEmpty{
                            dbRef.setValue(["data": hashId, "dataurl" : url.description, "message": message])
                        }else{
                            dbRef.setValue(["data": hashId, "dataurl" : url.description])
                        }
                        // Update table upload was finished
                        self.storedMessages[localIndex].setValue(true, forKey: "sentStatus")
                        self.storedMessages[localIndex].sentStatus = true
                        self.reloadMessages()
                    }
                }
            }
        }else if !message.isEmpty{
            // MARK: - Store message into FireBase database
            self.storedMessages[localIndex].sentStatus = true
            dbRef.setValue(["message": messageTextField.text!])
        }
    }
    
    // MARK: - Store message into DataCore
    func saveToLocal(message: String = String(), data: Data = Data(), sender: StoredUsers) -> StoredMessages{
        //MARK: - Create and populate the message object
        guard let context = context else { return StoredMessages() }
        let storedMessage = StoredMessages(entity : StoredMessages.entity(),insertInto: context)
        storedMessage.date = Date()
        storedMessage.user = sender
        storedMessage.chat = chat.chat
        if !message.isEmpty{ storedMessage.body = message }
        if !data.isEmpty{ storedMessage.data = data }
        try! context.save()
        return storedMessage
    }
    
    // MARK: - Store message into application data structure
    func saveToApp(message: StoredMessages) -> Int{
        storedMessages.append(message)
        return storedMessages.count - 1
    }
    
    func submitMessage(){
        self.messageTextField.becomeFirstResponder()
        if let image = messageImageView.image{
            if let data = image.jpegData(compressionQuality: 0.5){
                if messageTextField.hasText{
                    if let message = messageTextField.text{
                        // Save message and data
                        let sm = saveToLocal(message: message, data: data, sender: storedUser)
                        let idx = saveToApp(message: sm)
                        saveToServer(message: message, data: data, localIndex: idx)
                        // Clear the fields
                        messageTextField.text = ""
                        deleteImageAction()
                    }
                }else{
                    // Save only data
                    let sm = saveToLocal(data: data, sender: storedUser)
                    let idx = saveToApp(message: sm)
                    saveToServer(data: data, localIndex: idx)
                    // Clear the fields
                    deleteImageAction()
                }
            }
        }else if messageTextField.hasText{
            if let message = messageTextField.text{
                // Save only message
                let sm = saveToLocal(message: message, sender: storedUser)
                let idx = saveToApp(message: sm)
                saveToServer(message: message, localIndex: idx)
                // Clear the fields
                messageTextField.text = ""
            }
        }
    }
    
    func receiveMessages(){
        guard let storageRef = storageRef else { return }
        guard let databaseRef = databaseRef else { return }
        
        let dataRef = storageRef.child("images").child(storedUser.uid!).child(storedFriend.uid!)
        let dbRef = databaseRef.child("messages").child(storedUser.uid!).child(storedFriend.uid!)
        
        // MARK: - Get messages and data from CoreData Internal storage
        storedMessages = chat.messages
        
        // MARK: - Get messages from FireBase Database
        dbHandler = dbRef.observe(.childAdded, with: { (snapshot) in
            if let record = snapshot.value as? NSDictionary{
                let hashId = record["data"] as? String ?? ""
                let message = record["message"] as? String ?? ""
                if !hashId.isEmpty{
                    // MARK: - Get data from FireBase Storage
                    dataRef.child(hashId).getData(maxSize: 1 * 9000 * 9000) { data, error in
                            if let data = data{
                                if !message.isEmpty{
                                    let sm = self.saveToLocal(message: message, data: data, sender: self.storedFriend)
                                    _ = self.saveToApp(message: sm)
                                }else{
                                    let sm = self.saveToLocal(data: data, sender: self.storedFriend)
                                    _ = self.saveToApp(message: sm)
                                }
                                
                                // Delete the message from the server
                                snapshot.ref.removeValue()
                                dataRef.child(hashId).delete{ error in }
                            }
                      }
                }else if !message.isEmpty{
                    let sm = self.saveToLocal(message: message, sender: self.storedFriend)
                    _ = self.saveToApp(message: sm)
                    
                    // Delete the message from the server
                    snapshot.ref.removeValue()
                }
            }
        })
    }
    
    func reloadMessages(){
        chatsTableView.reloadData()
        if storedMessages.count > 2{
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.storedMessages.count-1, section: 0)
                self.chatsTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }

    override func viewWillDisappear(_ animated: Bool) {
        guard let databaseRef = databaseRef else { return }
        if let dbHandler = dbHandler{
            databaseRef.removeObserver(withHandle: dbHandler)
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Register the custom cells with a reuse identifier
        //chatsTableView.register(LeftMessageTableCell.self, forCellReuseIdentifier: "LeftMessageTableCell")
        //chatsTableView.register(RightMessageTableCell.self, forCellReuseIdentifier: "RightMessageTableCell")
        //chatsTableView.register(LeftPhotoTableCell.self, forCellReuseIdentifier: "LeftPhotoTableCell")
        
        self.adjustViewWhenKeyboardWillShow()
        
        imagePicker.delegate = self
        chatsTableView.delegate = self
        chatsTableView.dataSource = self
        messageTextField.delegate = self
        
        // MARK: - Create the date format
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        
        // MARK: - Create CoreData context
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        self.context = appDelegate.persistentContainer.viewContext
        
        // MARK: - Create the FireBase reference
        databaseRef = Database.database().reference()
        storageRef  = Storage.storage().reference()
        
        // MARK: - Get the storedFriend from the chat object
        let storedUsers = chat.chat.user!.allObjects as! [StoredUsers]
        storedFriend = storedUsers.first
        
        // MARK: - Create self CoreData user
        guard let context = context else { return }
        if let user = Auth.auth().currentUser{
            let request = StoredUsers.fetchRequest() as NSFetchRequest<StoredUsers>
            let predicate = NSPredicate(format: "uid == %@",user.uid)
            request.predicate = predicate
            if let storedUserFetch = try? context.fetch(request) as [StoredUsers]{
                if storedUserFetch.isEmpty{
                    let storedUser = StoredUsers(entity: StoredUsers.entity(),insertInto: context)
                    storedUser.displayName = user.uid
                    storedUser.email = user.email
                    storedUser.uid = user.uid
                    try? context.save()
                    self.storedUser = storedUser
                }else{
                    self.storedUser = storedUserFetch.first
                }
            }
        }
        
        // MARK: - Create the delete image button and add it to the image
        messageDeleteImageButton.setImage(messageDeleteImage, for: .normal)
        messageDeleteImageButton.addTarget(self, action: #selector(deleteImageAction), for: .touchUpInside)
        
        //imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(deleteImageAction)))
        messageImageView.isUserInteractionEnabled = true
        messageImageView.addSubview(messageDeleteImageButton)
        
        // MARK: - Add the UIImage object to the textField using a leftView object and resize textField
        messageLeftView.addSubview(messageImageView)
        
        // MARK: - Create constraints
        messageHeightConstraint = NSLayoutConstraint(item: messageTextField!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: messageLeftView.frame.height)
        messageTextField.addConstraint(messageHeightConstraint)
        messageTextField.leftView = messageLeftView;
        messageTextField.leftViewMode = UITextField.ViewMode.never
        messageHeightConstraint.isActive = false
        
        userDisplayNameLabel.text = storedFriend.displayName
        
        receiveMessages()
        
    }
}

extension ChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == messageTextField {
            submitMessage()
            return false
        }
        return true
    }
}

extension ChatViewController : UITableViewDataSource, UIScrollViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
 

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storedMessages.count
    }
    
    // MARK: - Determine which type of cell to use for each case
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let leftMessageCell = tableView.dequeueReusableCell(withIdentifier: "LeftMessageTableCell", for: indexPath) as! LeftMessageTableCell
        let rightMessageCell = tableView.dequeueReusableCell(withIdentifier: "RightMessageTableCell", for: indexPath) as! RightMessageTableCell
        let leftPhotoCell = tableView.dequeueReusableCell(withIdentifier: "LeftPhotoTableCell", for: indexPath) as! LeftPhotoTableCell
        let rightPhotoCell = tableView.dequeueReusableCell(withIdentifier: "RightPhotoTableCell", for: indexPath) as! RightPhotoTableCell
        let leftMessagePhotoCell = tableView.dequeueReusableCell(withIdentifier: "LeftMessagePhotoTableCell", for: indexPath) as! LeftMessagePhotoTableCell
        let rightMessagePhotoCell = tableView.dequeueReusableCell(withIdentifier: "RightMessagePhotoTableCell", for: indexPath) as! RightMessagePhotoTableCell
        
        // Reset the onSelect color
        leftMessageCell.selectionStyle = .none
        rightMessageCell.selectionStyle = .none
        leftPhotoCell.selectionStyle = .none
        rightPhotoCell.selectionStyle = .none
        leftMessagePhotoCell.selectionStyle = .none
        rightMessagePhotoCell.selectionStyle = .none
        
        let sender = (storedMessages[indexPath.row].user!.uid == storedUser.uid) ? storedUser.displayName : storedFriend.displayName
        
        switch sender{
        case storedUser.displayName:
            if let dataImage = storedMessages[indexPath.row].data{
                if let image = UIImage(data: dataImage){
                    if let message = storedMessages[indexPath.row].body{
                        leftMessagePhotoCell.message.text = message
                        leftMessagePhotoCell.imageview.image = image.resized(toWidth: leftMessagePhotoCell.imageview.frame.size.width)
                        if storedMessages[indexPath.row].sentStatus == false{
                            showActivityIndicatory(uiView: leftMessagePhotoCell.imageview, withLoadingContainer: false)
                        }else{
                            removeActivityIndicatory(uiView: leftMessagePhotoCell.imageview)
                        }
                        return leftMessagePhotoCell
                    }else{
                        leftPhotoCell.imageview.image = image.resized(toWidth: leftPhotoCell.imageview.frame.size.width)
                        if storedMessages[indexPath.row].sentStatus == false{
                            showActivityIndicatory(uiView: leftPhotoCell.imageview, withLoadingContainer: false)
                        }else{
                            removeActivityIndicatory(uiView: leftPhotoCell.imageview)
                        }
                        return leftPhotoCell
                    }
                }
            } else if let message = storedMessages[indexPath.row].body{
                leftMessageCell.message.text = message
                return leftMessageCell
            }
        case storedFriend.displayName:
            if let dataImage = storedMessages[indexPath.row].data{
                if let image = UIImage(data: dataImage){
                    if let message = storedMessages[indexPath.row].body{
                        rightMessagePhotoCell.message.text = message
                        rightMessagePhotoCell.imageview.image = image.resized(toWidth: rightMessagePhotoCell.imageview.frame.size.width)
                        return rightMessagePhotoCell
                    }else{
                        rightPhotoCell.imageview.image = image.resized(toWidth: rightPhotoCell.imageview.frame.size.width)
                        return rightPhotoCell
                    }
                }
            } else if let message = storedMessages[indexPath.row].body{
                rightMessageCell.message.text = message
                return rightMessageCell
            }
        case .none:
            break
        case .some(_):
            break
        }
        return UITableViewCell()
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        return UITableView.automaticDimension
     
     }
     
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dataImage = storedMessages[indexPath.row].data{
            if let image = UIImage(data: dataImage){
                self.imageTapped(image: image)
            }
        }
    }

    func imageTapped(image:UIImage){
        let scrollview = UIScrollView()
        dismissKeyboard()
        scrollview.frame = UIScreen.main.bounds
        scrollview.backgroundColor = .black
        
        scrollview.minimumZoomScale = 1
        scrollview.maximumZoomScale = 2
        
        scrollview.delegate = self as UIScrollViewDelegate
        
        let imageview = UIImageView(image: image.resized(toWidth: scrollview.frame.size.width))
        
        view.addSubview(scrollview)
        scrollview.addSubview(imageview)
        imageview.center = scrollview.center
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(_:)))
        scrollview.addGestureRecognizer(tap)
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        for subview in scrollView.subviews{
            if let imageview = subview as? UIImageView{
                let offsetX = max((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0)
                let offsetY = max((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0)
                imageview.center = CGPoint(x : scrollView.contentSize.width * 0.5 + offsetX, y : scrollView.contentSize.height * 0.5 + offsetY)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        dismissKeyboard()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        for subview in scrollView.subviews{
            if let imageview = subview as? UIImageView{
                return imageview
            }
        }
        return nil
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    func showActivityIndicatory(uiView: UIView, withLoadingContainer: Bool) {
        let container: UIView = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1).withAlphaComponent(0.3)
        container.tag = 123456
        

        let loadingView: UIView = UIView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.backgroundColor = withLoadingContainer ? #colorLiteral(red: 0.4756349325, green: 0.4756467342, blue: 0.4756404161, alpha: 1).withAlphaComponent(0.7) : .none
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10

        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.translatesAutoresizingMaskIntoConstraints = false
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actInd.style = .large

        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        
        NSLayoutConstraint.activate([
            container.widthAnchor.constraint(equalTo: uiView.widthAnchor, multiplier: 1),
            container.heightAnchor.constraint(equalTo: uiView.heightAnchor, multiplier: 1),
            
            loadingView.centerYAnchor.constraint(equalTo: container.centerYAnchor),
            loadingView.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            loadingView.widthAnchor.constraint(equalToConstant: 80),
            loadingView.heightAnchor.constraint(equalToConstant: 80),
            
            actInd.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor),
            actInd.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor),
            actInd.widthAnchor.constraint(equalToConstant: 40),
            actInd.heightAnchor.constraint(equalToConstant: 40),
        ])
        
        actInd.startAnimating()
    }
    
    func removeActivityIndicatory(uiView: UIView, withTag: Int = 123456) {
        if let activityIndicatory = uiView.viewWithTag(withTag){
            activityIndicatory.removeFromSuperview()
        }
    }
}
