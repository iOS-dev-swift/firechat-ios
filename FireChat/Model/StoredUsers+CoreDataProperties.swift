//
//  StoredUsers+CoreDataProperties.swift
//  FireChat
//
//  Created by Cohen, Dor on 02/10/2020.
//
//

import Foundation
import CoreData


extension StoredUsers {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredUsers> {
        return NSFetchRequest<StoredUsers>(entityName: "StoredUsers")
    }

    @NSManaged public var uid: String?
    @NSManaged public var displayName: String?
    @NSManaged public var email: String?
    @NSManaged public var messages: NSSet?
    @NSManaged public var chat: StoredChats?

}

// MARK: Generated accessors for messages
extension StoredUsers {

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: StoredMessages)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: StoredMessages)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSSet)

}

extension StoredUsers : Identifiable {

}
