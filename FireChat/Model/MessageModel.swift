//
//  MessageModel.swift
//  FireChat
//
//  Created by Cohen, Dor on 02/10/2020.
//
import UIKit

struct Message : Codable {
    var body: String?
    let date: Date
    let senderName: String?
}
