//
//  UserModel.swift
//  FireChat
//
//  Created by Cohen, Dor on 02/10/2020.
//
import UIKit

struct User{
    var displayName : String
    var email : String
    var UID : String
}
