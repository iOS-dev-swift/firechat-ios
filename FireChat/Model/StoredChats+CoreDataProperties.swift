//
//  StoredChats+CoreDataProperties.swift
//  FireChat
//
//  Created by Cohen, Dor on 02/10/2020.
//
//

import Foundation
import CoreData


extension StoredChats {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredChats> {
        return NSFetchRequest<StoredChats>(entityName: "StoredChats")
    }

    @NSManaged public var chatId: String?
    @NSManaged public var creationDate: Date?
    @NSManaged public var name: String?
    @NSManaged public var messages: NSSet?
    @NSManaged public var user: NSSet?

}

// MARK: Generated accessors for messages
extension StoredChats {

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: StoredMessages)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: StoredMessages)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSSet)

}

// MARK: Generated accessors for user
extension StoredChats {

    @objc(addUserObject:)
    @NSManaged public func addToUser(_ value: StoredUsers)

    @objc(removeUserObject:)
    @NSManaged public func removeFromUser(_ value: StoredUsers)

    @objc(addUser:)
    @NSManaged public func addToUser(_ values: NSSet)

    @objc(removeUser:)
    @NSManaged public func removeFromUser(_ values: NSSet)

}

extension StoredChats : Identifiable {

}
