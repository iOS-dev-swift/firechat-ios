//
//  StoredMessages+CoreDataProperties.swift
//  FireChat
//
//  Created by Cohen, Dor on 02/10/2020.
//
//

import Foundation
import CoreData


extension StoredMessages {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredMessages> {
        return NSFetchRequest<StoredMessages>(entityName: "StoredMessages")
    }

    @NSManaged public var body: String?
    @NSManaged public var data: Data?
    @NSManaged public var date: Date?
    @NSManaged public var sender: String?
    @NSManaged public var chat: StoredChats?
    @NSManaged public var user: StoredUsers?

}

extension StoredMessages : Identifiable {

}
